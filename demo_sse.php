<?php
header('Content-Type: text/event-stream');
header('Cache-Control: no-cache');

$id   = (! isset($_GET['id']) || $_GET['id']) ? '1' : $_GET['id'];
$time = date('Y-m-d H:i:s');
// echo "data: The server time is: {$time}\n\n";
// flush();

$db    = new PDO("mysql:host=localhost;dbname=test", "root", "");
$query = "select * from tb where id = ?";
$stmt  = $db->prepare($query);
$stmt->execute(array($id));
$result = $stmt->fetch(PDO::FETCH_OBJ);
$data = new stdClass();

$data->id        = $result->id;
$data->name      = $result->name;
$data->phone     = $result->phone;
$data->queryTime = $time;
$data->msg       = '';

echo "retry: 5000\n";

if ($data->phone != '0911000111') {
    $data->msg = 'changed';
    printf("data: " . json_encode($data) . "\n\n", $data->id);
} else {
    $data->msg = 'No change';
    printf("data: " . json_encode($data) . "\n\n", $data->id);
}

flush();
