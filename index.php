<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
    <style>
    .name { color: red; }
    .phone { color: blue; }
    .msg { color: #28AE07; }
    </style>
</head>
<body>
    <button onclick="disconnect()">disconnect</button>
    <button onclick="sayHi()">Notify me!</button>
    <div id="app">
    </div>

    <script>
        if(typeof(EventSource) !== 'undefined') {
            var source = new EventSource("demo_sse.php?id=1")
            source.onmessage = function(event) {
                var person = JSON.parse(event.data)
                document.getElementById("app").innerHTML += "<div><span class='name'>" + person.name + "</span>"
                + "<span class='phone'>" + person.phone + "</span>"
                + "<span class='msg'>" + person.msg + "</span></div>"
                // var n = new Notification("name:" + person.name + "\n\n" + '123')
            }
        } else {
            alert('Sorry! No server-sent events support')
        }

        function sayHi() {
            if (Notification && Notification.permission != 'granted') {
                Notification.requestPermission(function(status) {
                    if (Notification.permission !== status) {
                        Notification.permission = status
                    }
                })
            }

            var n = new Notification("hi");
        }

        function disconnect() {
            source.close()
            console.log('close')
        }
    </script>
</body>
</html>
